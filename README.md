# Atelier DIDAPRO Boyer-Moore

5 février 2020

## Ressources de l'atelier

- les fiches activité pour aborder l'algorithme de Boyer-Moore en débranché [activites.pdf](activites.pdf) (et le source LaTeX [activites.tex](activites.tex))
- un notebook Python codant les différentes constructions réalisées au cours les activités [boyer_moore.ipynb](boyer_moore.ipynb)

## Ressources complémentaires

- Le
  [site web de Thierry Lecroq](https://www-igm.univ-mlv.fr/~lecroq/string/node14.html#SECTION00140) (en anglais, avec code en C)
- Le [livre de Thierry Lecroq](http://igm.univ-mlv.fr/~mac/CHL/CHL.html) texte intégral en français disponible librement
- Le chapitre 10 du [livre Éléments d’algorithmique](http://www-igm.univ-mlv.fr/~berstel/Elements/Elements.pdf) de Berstel
- Le
    [support de cours](https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/blob/master/bloc4-5/algo-texte/algo-texte.pdf) donné en DIU Enseigner l'informatique au lycée par
    Mikaël Salson
